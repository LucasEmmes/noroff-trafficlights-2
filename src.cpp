enum LED{PedG=4, PedR=5, CarR=8, CarY=7, CarG=6};

const float GAMMA = 0.7;
const float RL10 = 50;

float get_lux() {
  int analogValue = analogRead(A0);
  float voltage = analogValue / 1024. * 5;
  float resistance = 2000 * voltage / (1 - voltage / 5);
  float lux = pow(RL10 * 1e3 * pow(10, GAMMA) / resistance, (1 / GAMMA));
  return lux;
}

bool direction = false;
bool night = false;

void turn_on_cars() {
  digitalWrite(LED::CarY, HIGH);
  delay(1000);
  digitalWrite(LED::CarR, LOW);
  digitalWrite(LED::CarY, LOW);
  digitalWrite(LED::CarG, HIGH);
}

void turn_off_cars() {
  digitalWrite(LED::CarG, LOW);
  digitalWrite(LED::CarY, HIGH);
  delay(1000);
  digitalWrite(LED::CarY, LOW);
  digitalWrite(LED::CarR, HIGH);
}

void turn_on_peds() {
  digitalWrite(LED::PedG, HIGH);
  digitalWrite(LED::PedR, LOW);
}

void turn_off_peds() {
  digitalWrite(LED::PedG, LOW);
  digitalWrite(LED::PedR, HIGH);
}

void button_pressed() {
  direction = true;
};

void night_mode() {
  digitalWrite(LED::CarG, LOW);
  digitalWrite(LED::CarY, HIGH);
  digitalWrite(LED::CarR, LOW);
  digitalWrite(LED::PedG, LOW);
  digitalWrite(LED::PedR, LOW);
  delay(500);
  digitalWrite(LED::CarY, LOW);
  delay(500);
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  for (int i = 4; i < 9; i++) {
    pinMode(i, OUTPUT);
  }
  pinMode(2, INPUT);
  attachInterrupt(digitalPinToInterrupt(2), button_pressed, RISING);

  digitalWrite(LED::CarG, HIGH);
}

void loop() {
  float lux = get_lux();

  if (lux < 10) {
    night = true;
    night_mode();
  }
  else if (direction) {
    delay(1000);
    turn_off_cars();
    delay(500);
    turn_on_peds();
    delay(5000);
    turn_off_peds();
    delay(500);
    turn_on_cars();
    direction = false;
  } else if (night) {
    turn_off_peds();
    turn_on_cars();
    night = false;
  }

}
